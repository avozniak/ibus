Rails.application.routes.draw do
  resources :routes
  root 'pages#welcome'
  get 'order', to: 'routes#order'

  resources :users, except: [:new]
  get 'sign_up', to: 'users#new'

  get 'login', to: 'sessions#new'
  post 'login', to: 'sessions#create'
  delete 'logout', to: 'sessions#destroy'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

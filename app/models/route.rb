class Route < ApplicationRecord
  has_and_belongs_to_many :orders

  validates :start, presence: true
  validates :finish, presence: true
  validates :departure, presence: true
  validates :arrival, presence: true
  validates :seats, presence: true, numericality: { greater_than: 0 }
  validates :price, presence: true, numericality: { greater_than: 0 }

  def self.departure_cities
    self.all.map {|route| route.start}.uniq
  end

  def self.arrival_cities
    self.all.map {|route| route.finish}.uniq
  end

  def departure_time
    departure.strftime("%m/%d/%y at %l:%M %p")
  end

  def arrival_time
    arrival.strftime("%m/%d/%y at %l:%M %p")
  end
end

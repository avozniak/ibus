class RoutesController < ApplicationController
  before_action :set_route, only: [:show, :edit, :update, :destroy]
  before_action :require_admin, only: [:new, :edit, :update, :destroy]

  def index
    @departure_cities = Route.departure_cities
    @arrival_cities = Route.arrival_cities
    if params[:departure_city]
      @routes = Route.where("start = ?", params[:departure_city])
    elsif params[:arrival_city]
      @routes = Route.where("finish = ?", params[:arrival_city])
    else
      @routes = Route.all
    end
  end

  def new
    @route = Route.new
  end

  def create
    @route = Route.new(route_params)
    if @route.save
      flash[:success] = "Route was successfully created"
      redirect_to routes_path
    else
      render 'new'
    end
  end

  def show
  end

  def edit
  end

  def update
    if @route.update(route_params)
      flash[:success] = "Route was succesfully updated"
      redirect_to @route
    else
      render 'edit'
    end
  end

  def destroy
    @route.destroy
    flash[:danger] = "Route was deleted"
    redirect_to routes_path
  end

  def order
    if session[:user_id]
      @route = Route.find(params[:route])
      @user = User.find(session[:user_id])
      @order = Order.new
      @order.user = @user
      if @route.seats > 0
        @order.routes << @route
        @route.seats -= 1
        @route.save
        @order.save
        flash[:success] = "Youre order was created"
        redirect_to routes_path
      else
        flash[:danger] = "Sorry, there is no free seats"
        redirect_to routes_path
      end
    else
      flash[:danger] = "You have to log in"
      redirect_to login_path
    end
  end

  private

  def route_params
    params.require(:route).permit(:start, :finish, :departure, :arrival, :price, :seats)
  end

  def set_route
    @route = Route.find(params[:id])
  end

  def require_admin
    if logged_in? and !current_user.admin?
      flash[:danger] = "Only admin can perform this action"
      redirect_to root_path
    end
  end
end


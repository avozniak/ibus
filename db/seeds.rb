# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Route.create(start: 'Kyiv', finish: 'Lviv', price: 100, seats: 40, departure: '20-05-2017 09:00', arrival: '20-05-2017 18:00')
Route.create(start: 'Lviv', finish: 'Ternopil', price: 70, seats: 40, departure: '20-05-2017 09:00', arrival: '20-05-2017 12:00')
class CreateRoutes < ActiveRecord::Migration[5.0]
  def change
    create_table :routes do |t|
      t.string :start
      t.string :finish
      t.datetime :departure
      t.datetime :arrival
      t.float :price
      t.integer :seats

      t.timestamps
    end
  end
end

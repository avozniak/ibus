class CreateOrdersRoutes < ActiveRecord::Migration[5.0]
  def change
    create_table :orders_routes, id: false do |t|
      t.belongs_to :order, index: true
      t.belongs_to :route, index: true
      t.timestamps
    end
  end
end

require 'rails_helper'
require 'spec_helper'

describe UsersController do
  context 'GET users#index' do
    before(:each) do
      get :index
    end
    it 'responds successfully with an HTTP 200 status code' do
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end
    it 'response headers is text/html' do
      expect(response.headers["Content-Type"]).to eq "text/html; charset=utf-8"
    end
    it 'loads all of the users into @users' do
      user1, user2 = User.create!(name: 'User 1', email: 'user1@mail.com', password: 'pass1'),
                     User.create!(name: 'User 2', email: 'user2@mail.com', password: 'pass2')
      expect(assigns(:users)).to match_array([user1, user2])
    end
    it 'renders the index template' do
      expect(response).to render_template(:index)
    end
  end

  context 'users#create' do
    let(:create) do
      @user =  User.create(name: 'User', email: 'user@email.com', password: 'password')
    end
    it 'Create a user' do
      expect{create}.to change{User.count}.by(1)
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    it 'response ok' do
      expect(response).to be_successful
    end
  end

  context 'users#new' do
    before(:each) { get :new }
    it 'assigns a new user to @users' do
      expect(assigns(:user)).to be_a_new(User)
    end
    it 'renders new template' do
      expect(response).to render_template(:new)
    end
  end
end
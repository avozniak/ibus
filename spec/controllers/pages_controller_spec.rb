require 'rails_helper'
require 'spec_helper'

RSpec.describe PagesController, type: :controller do
  context 'GET pages#welcome' do
    before(:each) do
      get :welcome
    end
    it 'responds successfully with an HTTP 200 status code' do
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end
    it 'response headers is text/html' do
      expect(response.headers["Content-Type"]).to eq "text/html; charset=utf-8"
    end

    it 'renders the welcome template' do
      expect(response).to render_template(:welcome)
    end
  end
end
